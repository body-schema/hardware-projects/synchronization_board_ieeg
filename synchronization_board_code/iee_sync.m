classdef iee_sync
  properties
    serial
    baudrate
    config_filename
  end
  methods
      function obj = iee_sync(com,baudrate,config_filename)
      obj.baudrate = baudrate;
      fprintf("Openning port %s, with baud rate %d\n",com,obj.baudrate);
      obj.serial = serialport(com,obj.baudrate);
      obj.config_filename = config_filename;
      configureTerminator(obj.serial,"LF","LF")
    end
    function r = set_config_to_device(obj)
      if(obj.config_filename ~= 0)
        fprintf("Opening file %s\n",obj.config_filename);
        f = fopen(obj.config_filename,'r');
        fprintf("Setting device\n");
        write(obj.serial,"SETTINGS","string");
        pause(1/1000);
        line = fgets(f);
        while(~feof(f))
          line = fgets(f);
          fprintf("%s",line);
          write(obj.serial,line,"string");
          pause(1/1000);  
        end
        write(obj.serial,"END","string");
        pause(1/1000);
        fprintf("Settings done\n");
        fclose(f)
        r = true;
      else
        r =  false;
      end
    end
    function r = write_paralel_port(obj,value)
     if(value >= 0 && value<=255)
        fprintf("Setting value %d\n",value);
        fprintf(obj.serial,"PORT\n%d\nEND\n",value);
        r = true;
      else
        fprintf("Value must be in range [0,255]\n");
        r =  false;
      end
    end
  end
end

