#include <ctype.h>
#include "pico/lock_core.h"
#include <Adafruit_NeoPixel.h>

#define FALLING_OFFSET 11
#define BUTTON_BASE 0
#define COAX_BASE 2
#define INPUT_BASE 3
#define RESTING_BASE 22
#define TIME_SLOT_PERIOD 2000
#define MESSAGE_READ_TIMEOUT 100000

const int buttons[2] = { 0, 1 };
const int coax = 2;
const int data_latch = 3;
const int data[8] = { 4, 5, 6, 7, 8, 9, 10, 11 };
const int inputs[8] = { 29, 28, 27, 26, 15, 14, 13, 12 };

std::queue<uint8_t> event_q;
int16_t events_list[23];

void gpio_callback(uint gpio, uint32_t events);
bool parse_config(String config_line, int16_t *events_list);

uint8_t state = 0;
int16_t setting = -1;
bool all_setup = false;

mutex_t q_lock;
uint16_t to_push = -1;

Adafruit_NeoPixel NeoPixel(1, 16, NEO_GRB + NEO_KHZ800);

void setup() {
  // delay(3000);
  mutex_init(&q_lock);
  NeoPixel.clear();
  NeoPixel.setPixelColor(0, NeoPixel.Color(0, 0, 255));
  NeoPixel.show();

  // set buttons
  for (uint8_t i = 0; i < 2; i++) {
    gpio_set_dir(buttons[i], false);           // set input
    gpio_set_pulls(buttons[i], false, false);  // set no pull-ups
    gpio_set_input_hysteresis_enabled(buttons[i], true);
    delay(100);
    gpio_set_irq_enabled_with_callback(buttons[i], GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_callback);  // enable irq
    //Serial.print("Button set up\n");
  }
  // set digital inputs
  for (uint8_t i = 0; i < 8; i++) {
    pinMode(inputs[i], INPUT_PULLUP);
    // gpio_set_dir(inputs[i], false);          // set input
    // gpio_set_pulls(inputs[i], true, false);  // set pull-ups
    gpio_set_input_hysteresis_enabled(inputs[i], true);
    gpio_set_irq_enabled_with_callback(inputs[i], GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_callback);  // enable irq
  }
  // set coax input
  gpio_set_dir(coax, false);          // set input
  gpio_set_pulls(coax, true, false);  // set pull-up
  gpio_set_input_hysteresis_enabled(coax, true);
  gpio_set_irq_enabled_with_callback(coax, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_callback);
  //set outputs
  for (uint8_t i = 0; i < 8; i++) {
    gpio_set_dir(data[i], true);                                 // set outputs
    gpio_set_drive_strength(data[i], GPIO_DRIVE_STRENGTH_12MA);  // set max 12mA drive strength
  }


  while (!all_setup)
    ;
}

void setup1() {

  Serial.flush();
  Serial.begin(115200);
  Serial.flush();
  Serial.setTimeout(1);

  // wait for config file to be recieved.
  bool config_recieved = false;
  bool settings_started = false;
  String line;
  String file;
  while (!config_recieved) {
    line = Serial.readStringUntil('\n');
    if (line == "SETTINGS") {
      settings_started = true;
    } else if (settings_started && line == "END") {
      config_recieved = true;
    } else if (settings_started && line.length() > 1) {
      settings_started = parse_config(line, events_list);
    }
  }
  all_setup = true;
}

void loop() {
  uint32_t to_set;
  if (!event_q.empty()) {  // if queue is not empty, get firts entry and set outputs
    mutex_enter_blocking(&q_lock);
    to_set = event_q.front();
    event_q.pop();
    mutex_exit(&q_lock);
    // set outputs
    setting = to_set;
    for (uint8_t i = 0; i < 8; i++) {
      digitalWrite(data[i], 0x01 & (to_set >> i));
    }
  } else {                                  // if queue is empty set resting code if requested
    if (events_list[RESTING_BASE] != -1) {  // if resting code is requested
      to_set = events_list[RESTING_BASE];
      for (uint8_t i = 0; i < 8; i++) {
        digitalWrite(data[i], 0x01 & (to_set >> i));
      }
    } else {
      //do nothing
    }
  }
  // wait for some miliseconds
  uint64_t start_of_waiting = time_us_64();
  while (time_us_64() - start_of_waiting < TIME_SLOT_PERIOD) {  // wait at leas 2000 us --> 2ms
  }
}

void loop1() {
  if (setting != -1) {
    Serial.printf("S%d\n", setting);
    setting = -1;
  }
  if (to_push != (uint16_t)-1) {
    mutex_enter_blocking(&q_lock);
    event_q.push(to_push);
    mutex_exit(&q_lock);
    to_push = -1;
  }
  uint64_t start_of_reading = time_us_64();
  if (Serial.available()) {
    String line = Serial.readStringUntil('\n');
    if (line.length() != 0 && line == "PORT") {
      state = 1;
      while (time_us_64() - start_of_reading < MESSAGE_READ_TIMEOUT) {  // Read at max 100000 us --> 100ms
        if (Serial.available()) {
          String line = Serial.readStringUntil('\n');
          while (line.length() == 0 && Serial.available()) {
            Serial.readStringUntil('\n');
            if (setting != -1) {
              Serial.printf("S%d\n", setting);
              setting = -1;
            }
            if (time_us_64() - start_of_reading < MESSAGE_READ_TIMEOUT) break;
          }
          if (line.length() != 0 && line == "END") {
            break;
          } else if (line.length() != 0 && state == 1) {
            mutex_enter_blocking(&q_lock);
            event_q.push(line.toInt());
            mutex_exit(&q_lock);
          }
        }
      }
      state = 0;
    }
  }
}

bool parse_config(String config_line, int16_t *events_list) {
  char buf[config_line.length() + 1];
  config_line.toCharArray(buf, config_line.length() + 1);
  uint8_t prev_i = 0;
  uint8_t i = config_line.indexOf(',', prev_i);
  String input = config_line.substring(prev_i, i);
  prev_i = i + 1;
  i = config_line.indexOf(',', prev_i);
  String rising = config_line.substring(prev_i, i);
  prev_i = i + 1;
  i = config_line.indexOf(',', prev_i);
  String falling = config_line.substring(prev_i, config_line.length());

  if (input == "input") return true;

  String category;
  uint8_t number;
  if (input.indexOf('_') != -1) {
    category = input.substring(0, input.indexOf('_'));
    number = input.substring(input.indexOf('_') + 1, input.length()).toInt();
  } else {
    category = input;
    number = 0;
  }

  uint8_t index = 0;
  if (category == "BUTTON") index += BUTTON_BASE - 1;
  else if (category == "IN") index += INPUT_BASE;
  else if (category == "COAX") index += COAX_BASE;
  else if (category == "RESTING") index += RESTING_BASE;
  index += number;
  events_list[index] = rising.toInt();
  if (category != "RESTING") events_list[index + FALLING_OFFSET] = falling.toInt();

  return true;
}

void gpio_callback(uint gpio, uint32_t events) {
  uint8_t index = 0;
  if (events == GPIO_IRQ_EDGE_RISE) {
    index += 0;
  } else if (events == GPIO_IRQ_EDGE_FALL) {
    index += FALLING_OFFSET;
  } else {
    return;
  }

  if (gpio == coax) index += COAX_BASE;  // COAX trigger
  else if (gpio <= 2) {                  // Button trigger
    index += BUTTON_BASE;
    index += (gpio == 0) ? 0 : 1;
  } else {  // input trigger
    index += INPUT_BASE;
    for (uint8_t i = 0; i < 8; i++) {  // find given input
      if (gpio == inputs[i]) {
        index += i;
        break;
      }
    }
  }

  to_push = events_list[index];

  // event_q.push(events_list[index]);
}