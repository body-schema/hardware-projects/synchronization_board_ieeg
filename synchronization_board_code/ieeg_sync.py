import serial as S
import time

class Ieeg_Sync:
    def __init__(self, com, rate = 115200, config_file = None):
        self.rate = rate
        print(f"Openning port {com}, with baud rate {self.rate}")
        self.serial = S.Serial(com,self.rate,timeout=0.01)
        self.config_filename = config_file

    def set_config_to_device(self):
        if self.config_filename is not None:
            print(f"Opening config file.")
            f = open(self.config_filename,'r')
            print(f"Setting up device.")
            self.serial.write("SETTINGS\n".encode())
            for line in f.readlines():
                print(line, end='')
                self.serial.write(line.encode())
            print()
            self.serial.write("END\n".encode())
            print(f"Settings done.")
            f.close()
            return True
        else:
            print(f"No config csv file provided!")
            return False

    def write_paralel_port(self,value):
        if value >= 0 and value <= 255:
            # print(f"Setting value {value}")
            self.serial.write(("PORT\n"+str(int(value))+'\n'+"END\n").encode())
            # self.serial.write((str(int(value))+'\n').encode())
            # self.serial.write("END\n".encode())
        else:
            print(f"Value must be in range [0,255]")



if __name__=="__main__":
    ieeg_sync = Ieeg_Sync("COM9", config_file='./default_config.csv')
    ieeg_sync.set_config_to_device()

    time.sleep(1)
    times = [-1 for i in range(256)]
    for i in range(256):

        start = time.time()
        ieeg_sync.write_paralel_port(i)
        # time.sleep(0.5)
        print(f"{ieeg_sync.serial.readline()}")
        timeDelta = (time.time()-start)*1000
        print(f"took: {timeDelta} ms")
        times[i] = timeDelta
        time.sleep(0.1)
    print(f"min: {min(times)} ms, max: {max(times)} ms, avg: {sum(times)/len(times)} ms")