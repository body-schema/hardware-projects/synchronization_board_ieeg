# synchronization_board_IEEG
This project implements simple board communicating with IEEG base unit 8-bit parallel bus to synchronize iEEG data with events during experiments.
Board can be commanded from PC over USB port using Python or Matlab script.

Board collects events and pushes them into the queue.
In fixed time intervals (2ms) it pops event from a queue and sets 8-bit bus to corresponding value.

There are two settings for the device to operate.
In first mode it sets a fixed value to the bus and leaves it until a new event is detected.
In the second device after a fixed time interval (2ms) for given event elapses and there are no more events waiting in the queue, the bus is returned to a resting value.
The resting value can be set by the user.

![function illustration](/doc/function.png)

## Power supply
Board is powered from a PC connected over a USB-C connector.

**DO NOT POWER THE DEVICE FROM ANY OTHER POWER SOURCE!**

On board is a LDO voltage regulator providing 3V3 (max. current 300mA) supply voltage for other electronic devices. 
The board also provides 5V voltage from the USB bus.


## Hardware details
The KiCad project can be found [here](https://gitlab.fel.cvut.cz/body-schema/hardware-projects/synchronization_board_ieeg/-/tree/main/ieeg_synchronization_board?ref_type=heads).

The board is controlled using an RP2040 microcontroller embedded into RP2040 Zero board.
Board utilizes two pushbuttons, SMA coaxial connector, 25 pin Dsub connector and pin header.
Detected events are coded and sent to an 8-bit parallel bus over 25 pin Dsub connector.
Pin header provides additional 8 digital inputs for custom event sources as well as 5V, 3V3 and GND connections.

Events are generated on digital inputs, push buttons, coaxial input or sent to the device over USB port.
Digital devices generate events on signal falling and rising edges (when buttons are pushed and released respectively).

Output and input voltage levels for all signals are 3V3 (High) and 0V (Low).
The only exception is SMA coaxial input, for these levels are 5V (High) and 0V (Low).
Singal from this connector is passed through the voltage divider to lower the voltage from 5V to 3V3.

In terms of malfunction protections, current sourced from RP2040Zero is limited by 470R resistors to protect the attached iEEG base unit.


![CAD visualization](/doc/wip.png)

To attach simple event sources such as buttons, switches etc, simply connect a switch between corresponding input pin and ground pin.
You can do so, as all input pins are pulled-up using internal pull-up resistors inside RP2040Zero.
Closing the switch connects the digital pin to the ground potential (generating falling edge).
Opening the switch disconnects.

## Software details
Codes are located [here](https://gitlab.fel.cvut.cz/body-schema/hardware-projects/synchronization_board_ieeg/-/tree/main/synchronization_board_code?ref_type=heads).
### Board program
Program for RP2040Zero is writen in Arduino IDE and source code can be found [here](https://gitlab.fel.cvut.cz/body-schema/hardware-projects/synchronization_board_ieeg/-/blob/main/synchronization_board_code/iee_sync/iee_sync.ino?ref_type=heads).


### Controlling SDK
For purposes of this project special classes for Matlab and Python were developed.
These classes implement all communication with the board.

Software currently features:
* setting 8-bit bus
* assigning code to events (see Section user manual)
* sending of current state being set to 8-bit bus over USB.

## User manual
At each power up device needs to be assigned events and codes for a given event.
This is done by filling table in the '.csv' file, which is then passed to the controlling class.
Default file name is `default_config.csv`.
This file can be edited and the program will automatically load it (unless set otherwise).
This file must be in the same directory as a given Matlab or Python script.
Codes are written into a corresponding column in decimal representation.

The last line of `.csv` table is `RESTING`.
This line sets the resting value to which board resets the bus after each event.
If `-1` is set, the device keeps the last event set on the bus until a new event is detected.

## Performance
We measured the time between sending a command to a board and receiving a message that the command was executed by a board.
We assume propagation time through the operational system should be the main source of delay.
Test was conducted on Windows 10 PC.

For Matlab mean measure time was ~10ms, whereas for Python script ~2ms.
With respect to 2ms time slots, Python latency is closing to zero.

Another test which was performed was jitter between event hapening on GPIO input and change of 8-bit bus state. 
Mean delay was about 1ms with STD of 0.6ms. 
the delay of 1ms is caused by 2ms wide impulses for the base unit.
