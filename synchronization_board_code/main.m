clear i_s;
i_s = iee_sync("COM9",115200,'./default_config.csv')
set_config_to_device(i_s)
%write_paralel_port(i_s,10)
pause(1);
times = zeros(1,256);
for r = 0:255
    start = posixtime(datetime("now"));
    write_paralel_port(i_s,r);
    ret = readline(i_s.serial);
    fprintf("%s\n",ret)
    timeDelta = (posixtime(datetime("now"))-start)*1000;
    fprintf("took: %i ms\n",timeDelta)
    times(r+1) = timeDelta;
    pause(.1);
end
fprintf("min: %i ms, max: %i ms, mean: %i\n",min(times),max(times), mean(times))
clear i_s;